﻿#
#
# Script that monitors different parts of the Biztalk connectivity.
#
# Author   : A. Eklind
# Date     : 2020-11-30
# Revision : Initial release
#
#
$ErrorActionPreference= 'silentlycontinue'
[bool] $bDeleteFile = $true
$smtpserver = 'smtp1.delaval.local'
$smtpport = 25
$subject = "TESTING: Connection issues detected on DEGLSRV653"
$body = "Check the attachment for details on the error."
$from = "no.reply@delaval.com"
#$to = @("andreas.eklind@delaval.com", "andreas.eklind@outlook.com")
#$cc = @("jonas.daven@delaval.com")
$to = @("dharmika.bandaru@capgemini.com", "nagendra.a.babu@capgemini.com")
$cc = @("jonas.daven@delaval.com", "andreas.eklind@delaval.com", "prashant.gujrathi@capgemini.com")
$saphostname = 'sappa1connect.delaval.local'
$sapport = 3307
$siebelhostname = 'defrsrv048.corp.delaval.com'
$siebelport = 3231
$logfilepath = "C:\temp\"
$logfile = 'monitoringLogfile_'
$eventlogfile = "eventLog_"
$sharename = "\\DEGLSRV653\monitoring\"
$fileextension = '.txt'
$StartTime = (Get-Date).AddHours(-2)
$yesterday = (Get-Date).AddDays(-1)
$hourofday = Get-Date -Format HHmm
$TimeStamp = [DateTime]::Now.ToString("yyyyMMdd-HHmmss")
$logfilename = $logfilepath + $logfile + $TimeStamp + $fileextension
$csvfilename = $logfilepath + $eventlogfile + $TimeStamp + ".csv"
$fullEventLog = $logfilepath + "fullEventLog_" + $TimeStamp + ".csv"
#
# Error Code information
#
# 5742 = Initiating communication to backup channel (InstanceId = 2160072302)
#
$logfilter =@{
	LogName='Application'
	Data='sndBAPIRequest_To_BAPIResponse'
	Id='5742'
	StartTime=$StartTime
}
$logfilterFull = @{
	LogName='Application'
	ProviderName='Biztalk Server'
	StartTime=$yesterday
}
# Dump the full eventLog to the monitoring share once a day
if ( $hourofday -eq "0100" ) {
    echo "$TimeStamp - Dumping yesterdays EventLog to the share." | Out-File $logfilename -Append 
    Get-WinEvent -FilterHashtable $logfilterFull | Export-Csv -Path $csvfilename -Delimiter ";"
}
#
echo "$TimeStamp - Application Log Check" | Out-File $logfilename -Append 
#
Get-WinEvent -FilterHashtable $logfilter | Export-Csv -Path $csvfilename -Delimiter ";"
If ($?) {
    [bool] $bDeleteFile = $false
    $TimeStamp = [DateTime]::Now.ToString("yyyyMMdd-HHmmss")
    $temp = $csvfilename.Replace($logfilepath,$sharename)
    echo "$TimeStamp - Entries found, see $temp."| Out-File $logfilename -Append 
} Else {
    [bool] $bDeleteFile = $true
    $TimeStamp = [DateTime]::Now.ToString("yyyyMMdd-HHmmss")
    echo "$TimeStamp - No entries found."| Out-File $logfilename -Append 
}
$TimeStamp = [DateTime]::Now.ToString("yyyyMMdd-HHmmss")
echo "$TimeStamp - Connection Check"| Out-File $logfilename -Append 
$connectResult = Test-NetConnection -ComputerName $saphostname -Port $sapport -InformationLevel Quiet
If ($connectResult) {
    $TimeStamp = [DateTime]::Now.ToString("yyyyMMdd-HHmmss")
    echo "$TimeStamp - Connection to $saphostname on port $sapport successful." | Out-File $logfilename -Append
    if ( $bDeleteFile -ne $false ) {
        [bool] $bDeleteFile = $True
    }
} Else {
    $TimeStamp = [DateTime]::Now.ToString("yyyyMMdd-HHmmss")
    echo "$TimeStamp - Connection failed to $saphostname on port $sapport, verify DNS" | Out-File $logfilename -Append
    Resolve-DnsName $saphostname | Out-File $logfilename -Append
    if ($?) {
        $TimeStamp = [DateTime]::Now.ToString("yyyyMMdd-HHmmss")
        echo "$TimeStamp - DNS Resolved SAP host correctly" | Out-File $logfilename -Append
        if ( $bDeleteFile -ne $false ) {
            [bool] $bDeleteFile = $true
        }
    } Else {
        $TimeStamp = [DateTime]::Now.ToString("yyyyMMdd-HHmmss")
        echo "$TimeStamp - DNS Resolution SAP host failed" | Out-File $logfilename -Append
        [bool] $bDeleteFile = $false
        #
        # Also verify DNS resolution to Siebel server
        #
        $TimeStamp = [DateTime]::Now.ToString("yyyyMMdd-HHmmss")
        echo "$TimeStamp - DNS resolve Siebel host" | Out-File $logfilename -Append
        Resolve-DnsName $siebelhostname | Out-File $logfilename -Append
        if ($?) {
            $TimeStamp = [DateTime]::Now.ToString("yyyyMMdd-HHmmss")
            echo "$TimeStamp - DNS Resolved Siebel host correctly" | Out-File $logfilename -Append
        } Else {
            $TimeStamp = [DateTime]::Now.ToString("yyyyMMdd-HHmmss")
            echo "$TimeStamp - DNS Resolution Siebel host failed" | Out-File $logfilename -Append
        }
    }
}
if ($bDeleteFile) {
    Remove-Item -Path $logfilename
    Remove-Item -Path $csvfilename
} Else {
    Send-MailMessage -From $from -Subject $subject -To $to -Cc $cc -Body $body -Attachments $logfilename -SmtpServer $smtpserver -Port $smtpport
}
