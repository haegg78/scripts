﻿#
#
# Script that executes multiple connectivity checks.
#
# Author: A.Eklind
#
# Variables
$saphostname = 'sappa1connect.delaval.local'
$sapport = 3307
$siebelhostname = 'defrsrv048.corp.delaval.com'
$siebelport = 3231
$StartTime = (Get-Date).AddDays(-2)
$eventInstanceId = 2160072302

# Verify connection to SAP GW
echo "Testing connection to $saphostname`:$sapport"
Test-NetConnection -ComputerName $saphostname -Port $sapport

# Verify DNS resolution of SAP GW
echo "Trying to resolve host $saphostname"
Resolve-DnsName $saphostname

# Verify connection to Siebel Host
echo "Testing connection to $siebelhostname`:$siebelport"
Test-NetConnection -ComputerName $siebelhostname -Port $siebelport

# Verify DNS resolution of Siebel Host
echo "Trying to resolve host $siebelhostname"
Resolve-DnsName $siebelhostname

# List all events from the EventLog from the last 2 days
echo "Listing all events from the eventlog, with InstanceId`: $eventInstanceId and StartTime`: $StartTime"
Get-EventLog -LogName Application -InstanceId $eventInstanceId -After $StartTime
