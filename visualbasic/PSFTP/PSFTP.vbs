'* ************************************************************************************************************************
'*
'* Titel            : Dateiübertragung via PuTTY Secure File Transfer (SFTP) client
'*
'* Beschreibung 	: Hiermit kann eine Datei via PSFTP gesendet oder auch mehrere abgeholt werden.
'*                  
'* Aufruf	      	: via WF DL_SendingTreatment oder DL_POLL_PSFTP
'*			
'*			          Allgemeine Parameter:
'*						/Variant:[SEND|RECEIVE|RECEIVE_SINGLE|DIR]
'*						/PartnerNr:{FTP-Partnername im BIS}
'*						/HostPath:{Hostpfad inkl. Dateiname/Dateimaske im Zielsystem}
'*						/WFID:{Workflow-ID}
'*						/LogFile:{Protokolldatei inkl. Pfad}
'*						/PreDirList:{wenn gefüllt, dann wird vorher ein DirList vom HostPath durchgeführt}
'*						/UseDirFileList:{wenn 'X' dann wird beim Dir zusätzlich eine Dateiliste erzeugt}
'*			          Parameter bei SEND:
'*						/SourcePath:{Quelldatei inkl. Pfad}
'*						/TempPrefix:{Standard ist "temp_"}
'*						/TempSuffix:{Standard ist ".tmp"}
'*			          Parameter bei RECEIVE:
'*						/JobID:{Schedule-ID/Name}
'*						/DestPath:{Zielverzeichnis (Port)}
'*						/DestPrefix:{Zielprefix}
'*			          Zusätzlich Parameter bei RECEIVE_SINGE:
'*						/MaxFiles:{maximale Anzahl Dateien die abgeholt werden sollen, Standard ist 1000}
'*			
'* Vorraussetzungen	: In BIS muss der FTP Partner mit dem HostSystem 'PSFTP' vorhanden sein
'*			
'* Version      	: 2011-07-27 09:00, Jörg Weller
'*
'* History          : 2011-09-22 10:00, WellJo, FTPcmd überarbeitet
'*										Beim RECEIVE konnte ggf. ein Abbruch erfolgen, wenn zu viele
'*										Dateien im Quell-Verzeichnis vorhanden waren.
'*					  2011-09-23 15:30, WellJo, Variante RECEIVE_SINGE hinzugefügt
'*					  2020-11-17 16:00, EkliAn, Added handling for dynamically handling delete file option. Solving issue
'*												with FEDEX SFTP.
'*
'* ************************************************************************************************************************

Option Explicit         ' Variablen-Deklaration ist hiermit Pflicht !
On Error Resume Next    ' Fehler müssen vom Skript behandelt werden !

Const ERRORCODE = 1		' Fehlercode zur BIS-Rückmeldung
Const OKCODE = 0		' OK-Code zur BIS-Rückmeldung

Dim sobjApp				' Objekt, dass Standard-Routinen enthält
Dim sobjPSFTP           ' PSFTP-Handling
Dim sstrLockFile        ' Dateiname+Pfad der Empfangs-Script-Sperrdatei (Job darf ggf. nicht mehrmals parallel laufen)
Dim sobjLockFile        ' Empfangs-Script-Sperrdatei (geöffnet während der Laufzeit)

' Application initialisieren
Set sobjApp = New WApplication
sobjApp.init
sobjApp.PrintStartInfo
	
' ... PSFTP vorbereiten
Set sobjPSFTP = New PSFTP
If Not sobjPSFTP.init() Then
	sobjApp.Log.Write sobjApp.Log.ErrorPrefixText & "Error occurred!" 
    WScript.Quit ERRORCODE
End If
sobjPSFTP.UseDirFileList = ( sobjApp.Arg( "UseDirFileList" ) = "X" )

' Start-Auswahl
Select Case UCase( sobjApp.Arg( "VARIANT" ))

	' Eine Datei senden
	Case "SEND" 
		sobjApp.Log.Write "PartnerNr   = " & sobjApp.Arg( "PartnerNr" )
		sobjApp.Log.Write "HostPath    = " & sobjApp.Arg( "HostPath" )
		sobjApp.Log.Write "SourcePath  = " & sobjApp.Arg( "SourcePath" )
		sobjApp.Log.Write "WfID        = " & sobjApp.Arg( "WFID" )
		' FTP Partner-Daten ermitteln
		If sobjPSFTP.LoadPartnerData( sobjApp.Arg( "PartnerNr" )) Then
			' Auch als PSFTP-Partner definiert?
			' ( Auswahl ist in "D:\BIS\user\forms\frmPartnerFTP.xml" definiert. )
			If sobjPSFTP.HostSystem = 1 Then
				' Ggf. vorher ein DirList vom HostPath durchgeführen
				If sobjApp.Arg( "PreDirList" ) <> "" Then
					sobjPSFTP.FTPdir sobjApp.Arg( "HostPath" )
				End If
				' jetzt Senden
				If Err = 0 Then
					sobjPSFTP.FTPsend sobjApp.Arg( "SourcePath" ), sobjApp.Arg( "HostPath" )
				End If
			Else
				Err.Raise 1001, "Select", "Partner is not defined as PSFTP-Partner!"
			End If
		Else
			Err.Raise 1001, "Select", "Load PartnerData: " & sobjApp.Arg( "PartnerNr" )
		End If
		
	' Eine oder mehrere Dateien abholen (via MGET)
	Case "RECEIVE" 
		sobjApp.Log.Write "PartnerNr         = " & sobjApp.Arg( "PartnerNr" )
		sobjApp.Log.Write "HostPath          = " & sobjApp.Arg( "HostPath" )
		sobjApp.Log.Write "WfID              = " & sobjApp.Arg( "WFID" )
		sobjApp.Log.Write "JobID             = " & sobjApp.Arg( "JobID" )
		sobjApp.Log.Write "DestPath+Prefix   = " & sobjApp.Arg( "DestPath" ) & "\" & sobjApp.Arg( "DestPrefix" )
		' Sperrdatei erzeugen
		sstrLockFile = sobjApp.RootPath & "\work\" & sobjApp.Arg( "JobID" ) & ".lock"
		Set sobjLockFile = New LockFile		
        If Not sobjLockFile.Open( sstrLockFile, "locked" ) Then
            Err.Raise 1001, "Select", "Lockfile not created : " & sstrLockFile
		Else
			sobjApp.Log.Write "* Application-Lockfile is " & sstrLockFile
			' FTP Partner-Daten ermitteln
			If sobjPSFTP.LoadPartnerData( sobjApp.Arg( "PartnerNr" )) Then
				' Auch als PSFTP-Partner definiert?
				' ( Auswahl ist in "D:\BIS\user\forms\frmPartnerFTP.xml" definiert. )
				If sobjPSFTP.HostSystem = 1 Then
					' Ggf. vorher ein DirList vom HostPath durchgeführen
					If sobjApp.Arg( "PreDirList" ) <> "" Then
						sobjPSFTP.FTPdir sobjApp.Arg( "HostPath" )
					End If
					' Jetzt Datei(en) anholen
					If Err = 0 Then
						sobjPSFTP.FTPreceive sobjApp.Arg( "HostPath" ), _
											 sobjApp.Arg( "DestPath" ) & "\" & sobjApp.Arg( "DestPrefix" )
					End If
				Else
					Err.Raise 1001, "Select", "Partner is not defined as PSFTP-Partner!"
				End If
			Else
				Err.Raise 1001, "Select", "Load PartnerData: " & sobjApp.Arg( "PartnerNr" )
			End If
        End If

	' Eine oder mehrere Dateien NACHEINANDER abholen ( via GET, inkl. dir )
	Case "RECEIVE_SINGLE" 
		sobjApp.Log.Write "PartnerNr         = " & sobjApp.Arg( "PartnerNr" )
		sobjApp.Log.Write "HostPath          = " & sobjApp.Arg( "HostPath" )
		sobjApp.Log.Write "WfID              = " & sobjApp.Arg( "WFID" )
		sobjApp.Log.Write "JobID             = " & sobjApp.Arg( "JobID" )
		sobjApp.Log.Write "DestPath+Prefix   = " & sobjApp.Arg( "DestPath" ) & "\" & sobjApp.Arg( "DestPrefix" )
		' Sperrdatei erzeugen
		sstrLockFile = sobjApp.RootPath & "\work\" & sobjApp.Arg( "JobID" ) & ".lock"
		Set sobjLockFile = New LockFile		
        If Not sobjLockFile.Open( sstrLockFile, "locked" ) Then
            Err.Raise 1001, "Select", "Lockfile not created : " & sstrLockFile
		Else
			sobjApp.Log.Write "* Application-Lockfile is " & sstrLockFile
			' FTP Partner-Daten ermitteln
			If sobjPSFTP.LoadPartnerData( sobjApp.Arg( "PartnerNr" )) Then
				' Auch als PSFTP-Partner definiert?
				' ( Auswahl ist in "D:\BIS\user\forms\frmPartnerFTP.xml" definiert. )
				If sobjPSFTP.HostSystem = 1 Then
					' Jetzt Datei(en) anholen
					If Err = 0 Then
						sobjPSFTP.FTPreceiveSingle sobjApp.Arg( "HostPath" ), _
											 sobjApp.Arg( "DestPath" ) & "\" & sobjApp.Arg( "DestPrefix" )
					End If
				Else
					Err.Raise 1001, "Select", "Partner is not defined as PSFTP-Partner!"
				End If
			Else
				Err.Raise 1001, "Select", "Load PartnerData: " & sobjApp.Arg( "PartnerNr" )
			End If
        End If
		
	' Inhalt eines Verzeichnis auslesen
	Case "DIR" 
		sobjApp.Log.Write "PartnerNr         = " & sobjApp.Arg( "PartnerNr" )
		sobjApp.Log.Write "HostPath          = " & sobjApp.Arg( "HostPath" )
		sobjApp.Log.Write "WfID              = " & sobjApp.Arg( "WFID" )
		sobjApp.Log.Write "JobID             = " & sobjApp.Arg( "JobID" )
		' FTP Partner-Daten ermitteln
		If sobjPSFTP.LoadPartnerData( sobjApp.Arg( "PartnerNr" )) Then
			' Auch als PSFTP-Partner definiert?
			' ( Auswahl ist in "D:\BIS\user\forms\frmPartnerFTP.xml" definiert. )
			If sobjPSFTP.HostSystem = 1 Then
				' jetzt Verzeichnis auslesen
				sobjPSFTP.FTPdir sobjApp.Arg( "HostPath" )
			Else
				Err.Raise 1001, "Select", "Partner is not defined as PSFTP-Partner!"
			End If
		Else
			Err.Raise 1001, "Select", "Load PartnerData: " & sobjApp.Arg( "PartnerNr" )
		End If

	' Falscher Aufruf!
	Case Else
		Err.Raise 1001, "Select", "Unknown Variant: " & sobjApp.Arg( "VARIANT" )
		
End Select

' Sind Fehler aufgetreten?
If Err Then
	sobjApp.Log.Write sobjApp.Log.ErrorPrefixText & "Error occurred!" 
	WScript.Quit ERRORCODE
Else
	WScript.Quit OKCODE
End If

'* ************************************************************************************************************************
'* PSFTP-Handling
'*

Class PSFTP 

	Public BISRootPath
	Public WorkflowUDLFile
	Public WorkflowDB
	Public Host
	Public HostSystem
	Public User
	Public PW
	Public HostPath
	Public CmdFile
	Public FTPEXE
	Public FtpText
	Public FtpCodeList
	Public DirFileList
	Public UseDirFileList
	Public DirFileListCount
	Public blnDeleteFileSetting
    
	'----------------------------------------------------------------------------------------------

    Private Sub Class_Initialize()
        On Error Resume Next

		BISRootPath = sobjApp.Env( "BISROOT" )
		WorkflowUDLFile = sobjApp.Name & "_Workflow"
		Set WorkflowDB = Nothing
		UseDirFileList = False

		FTPEXE = sobjApp.RootPath & "\psftp\prog\psftp.exe"
        
    End Sub

	'----------------------------------------------------------------------------------------------

    Private Sub Class_Terminate()
        On Error Resume Next

		Set WorkflowDB = Nothing
	
    End Sub

	'----------------------------------------------------------------------------------------------

	Public Function init()
        On Error Resume Next

		Set WorkflowDB = New WOLEDB
		If Not WorkflowDB.OpenUDL( WorkflowUDLFile ) Then
            init = False
			Exit Function
		End If
		
        init = True
        
	End Function
    
	'----------------------------------------------------------------------------------------------
	
	Public Function LoadPartnerData( strPartner )
        On Error Resume Next

		Dim rstPartner
		Dim intExists
		
		LoadPartnerData = False
		intExists = 0
		
        Set rstPartner = WorkflowDB.OpenRecordset( _
            "SELECT * FROM P_DfuePartner_FTP" & _
            " WHERE (PartnerNr = '" & strPartner & "')", False )
        If Err Then	Exit Function
        
        Do Until rstPartner.EOF
			intExists = 1
            Host = WorkflowDB.GetValue( rstPartner, "HostName" )
			If Host = "" Then
				Host = WorkflowDB.GetValue( rstPartner, "HostIP_3" ) & "." & _
					   WorkflowDB.GetValue( rstPartner, "HostIP_2" ) & "." & _
					   WorkflowDB.GetValue( rstPartner, "HostIP_1" ) & "." & _
					   WorkflowDB.GetValue( rstPartner, "HostIP_0" )
			End If
            User = WorkflowDB.GetValue( rstPartner, "UserName" )
            PW = WorkflowDB.GetValue( rstPartner, "Passwort" )
            HostPath = WorkflowDB.GetValue( rstPartner, "HostVerz" )
            HostSystem = WorkflowDB.GetValue( rstPartner, "HostSystem" )
			If WorkflowDB.GetValue( rstPartner, "DeleteFile" ) = "Y" Then
				blnDeleteFileSetting = True
			Else
				blnDeleteFileSetting = False
			End If
			sobjApp.Log.Write vbNewLine & _
				"+++ DeleteFile option is: " & blnDeleteFileSetting
            Exit Do
            'rstPartner.MoveNext
        Loop
        rstPartner.Close
		
		If Err Then	Exit Function
		If intExists = 0 Then Exit Function
		
		LoadPartnerData = True
        
	End Function
    
	'----------------------------------------------------------------------------------------------
    ' Inhalt eines Verzeichnis auslesen (Pfad &  Dateimaske möglich)
	'
	Public Function FTPdir( strPath )
		On Error Resume Next

		Dim strDestPath
		Dim strDestName
		Dim strCmdList
		Dim strLine
		Dim strLines
		Dim blnStart
		Dim intPos
		Dim intFiles
		Dim strFileName

		' Auflistung der gefundenen Dateien initialisieren
        Set DirFileList = CreateObject( "Scripting.Dictionary" )
        DirFileList.CompareMode = vbTextCompare
		DirFileListCount = 0
		
		' Angaben zur Quelle in Pfad und Dateiname aufteilen
		strDestPath = sobjApp.FSO.GetParentFolderName( strPath )		
		strDestName = sobjApp.FSO.GetFileName( strPath )		

		' FTP Kommandos definieren
		' Es darf NIE die option "-be" (continue batch processing on errors) in FTPcmd
		' verwendet werden!
		strCmdList = "cd """ & strDestPath & """" & vbNewLine & _
					 "pwd" & vbNewLine & _
					 "dir """ & strDestName & """" & vbNewLine & _
					 "bye" & vbNewLine & _
					 "quit" & vbNewLine

		' FTP ausführen
		If Not FTPcmd( strCmdList ) Or Err Then
		   Exit Function
		End If

		' Wurden alle FTP Befehle abgearbeitet?
		If InStr( FtpCodeList, "cd;pwd;dir;bye" ) <= 0 Then
 		    sobjApp.Log.Write vbNewLine & _
			 		 "+++ DirList NOT successfully completed" & _
					 vbNewLine
			Err.Raise vbObjectError + 50, "", _
					  "DirList Error"
		   Exit Function
		End If
		
		' Jetzt ggf. die Dateien aus dem DIR ermitteln
		If UseDirFileList Then
			sobjApp.Log.Write vbNewLine & "Listing"
			strLines = Split( FtpText, vbNewLine )
			blnStart = False
			intFiles = 0
			For Each strLine In strLines
				' Erst beginnen wenn der "Start-Text" gefunden wurde!
				If blnStart Then
					' Position des Uhrzeit-Trennzeichens ermitteln...
					' ( ist einfacher als alle möglichen Felder vorher zu prüfen )
					intPos = InStr( strLine, ":" )
					If intPos > 10 _
					And LCase( Left( strLine, 1 )) <> "d" Then
						' ...hinter der Uhrzeit "hh:mm " folgt der Dateiname
						' und wenn am Anfang kein "d" (Directory) steht...
						strFileName = Mid( strLine, intPos + 4 )
						Select Case strFileName
							Case ".", ".."
								' keine Datei
							Case Else
								' jep, eine Datei
								intFiles = intFiles + 1
								If DirFileList( strFileName ) = "" Then
									DirFileList( strFileName ) = strLine
									DirFileListCount = DirFileListCount + 1
									sobjApp.Log.Write " file = " & strFileName
								Else
									sobjApp.Log.Write vbNewLine & _
											 "+++ Wrong DirFileList (Entry already exists)" & _
											 vbNewLine
									Err.Raise vbObjectError + 50, "", _
											  "DirList Error"
									Exit Function
								End If
						End Select
					End If
				Else
					' "Start-Text" vorhanden?
					If Left( strLine, 17 ) = "Listing directory" Then
						blnStart = True
					End If
				End If
			Next

			sobjApp.Log.Write vbNewLine & " Files = " & intFiles & vbNewLine
			
		End If
		
	End Function

	'----------------------------------------------------------------------------------------------
	' Eine oder mehrere Dateien NACHEINANDER abholen ( via GET, inkl. dir )
	'
	Public Function FTPreceiveSingle( strHostFilePath, strDestPathAndPrefix )
		On Error Resume Next

		Dim strTempPath
		Dim strHostPath
		Dim strHostMask
		Dim strFile
		Dim strCmdList
		Dim intFile
		Dim intMaxFiles
		
		Dim strLines
		Dim strLine
		Dim blnMGET
		Dim strLocalFile
		Dim objFileList
		Dim intTransfered
		Dim intDeleted
		
		' Temp-Verzeichnis festlegen und ggf. erstellen
		strTempPath = sobjApp.RootPath & "\work\" & sobjApp.Arg( "JobID" )
		If Not sobjApp.FSO.FolderExists( strTempPath ) Then
			sobjApp.FSO.CreateFolder strTempPath
		End If
		If Err Then Exit Function

		' Angaben zur Quelle in Pfad und Dateiname aufteilen
		strHostPath = sobjApp.FSO.GetParentFolderName( strHostFilePath )		
		strHostMask = sobjApp.FSO.GetFileName( strHostFilePath )		

		' Max. Anzahl Dateien festlegen
		If sobjApp.Arg( "MaxFiles" ) = "" Then
			intMaxFiles = 1000
		Else
			intMaxFiles = CInt( sobjApp.Arg( "MaxFiles" ))
		End If
		
		' jetzt geht der FTP los
		sobjApp.Log.Write vbNewLine & "Start receive from remote side"
		
		' Ersteinmal den Inhalt abfragen
		UseDirFileList = True
		sobjPSFTP.FTPdir strHostFilePath
		If Err Then
			Exit Function
		End If
		
		' Jetzt jede gefundene Datei einzeln verarbeiten
		intFile = 0
		For Each strFile In DirFileList
		
			' max. Anzahl schon erreicht?
			intFile = intFile + 1
			If intFile > intMaxFiles Then
				sobjApp.Log.Write "...MaxFiles reached!"
				Exit Function
			End If
		
			sobjApp.Log.Write vbNewLine & "New file: " & strFile & vbNewLine
			
			' FTP Kommandos definieren
			' Es wird bei jeder Datei eine neue FTP-Session genutzt!
			' Nach dem GET erfolgt gleich ein DEL! Dies ist kein Risiko da PSFTP abbricht,
			' sobald ein Befehl fehlschlägt. 
			' Es darf aber NIE die option "-be" (continue batch processing on errors) in FTPcmd
			' verwendet werden!
			strCmdList = "cd """ & strHostPath & """" & vbNewLine & _
						 "lcd """ & strTempPath & """" & vbNewLine & _
						 "get """ & strFile & """" & vbNewLine & _
						 "del """ & strFile & """" & vbNewLine & _
						 "bye" & vbNewLine & _
						 "quit" & vbNewLine
						 
			' FTP ausführen
			If Not FTPcmd( strCmdList ) Or Err Then
			   Exit Function
			End If

			' Wurden alle FTP Befehle abgearbeitet?
			If InStr( FtpCodeList, "cd;lcd;get;del;bye" ) <= 0 Then
				sobjApp.Log.Write vbNewLine & _
						 "+++ Transfer NOT successfully completed" & _
						 vbNewLine
				Err.Raise vbObjectError + 50, "", _
						  "Transfer Error"
			   Exit Function
			End If

			' Lokale Temp-Datei auch wirklich da?
			If Not sobjApp.FSO.FileExists( strTempPath & "\" & strFile ) Then			
				sobjApp.Log.Write vbNewLine & _
						 "+++ Local temp file not exists!" & _
						 vbNewLine
				Err.Raise vbObjectError + 50, "", _
						  "Transfer Error"
			   Exit Function
			End If
			
			' Erfolgreich abgeholt und jetzt ins eigentliche Ziel verschieben!
			sobjApp.Log.Write "File received successfully to temp path."
			sobjApp.FSO.MoveFile strTempPath & "\" & strFile, strDestPathAndPrefix & strFile
			
			If Err Then
				Exit Function
			End If

			sobjApp.Log.Write "File moved successfully to process path."

			sobjApp.Log.Write "...Files received = " & intFile & " of " & DirFileListCount & _
				" ( but max. " & intMaxFiles & " )"
			
		Next
		
		sobjApp.Log.Write "...Files received = " & intFile
		
	End Function
	
	'----------------------------------------------------------------------------------------------
	' Eine oder mehrere Dateien abholen (via MGET)
	'
	Public Function FTPreceive( strHostFilePath, strDestPathAndPrefix )
		On Error Resume Next

		Dim strTempPath
		Dim strHostPath
		Dim strHostMask
		Dim strCmdList
		Dim strLines
		Dim strLine
		Dim blnMGET
		Dim strLocalFile
		Dim objFileList
		Dim strFile
		Dim intTransfered
		Dim intDeleted
		Dim intFile
		
		' Temp-Verzeichnis festlegen und ggf. erstellen
		strTempPath = sobjApp.RootPath & "\work\" & sobjApp.Arg( "JobID" )
		If Not sobjApp.FSO.FolderExists( strTempPath ) Then
			sobjApp.FSO.CreateFolder strTempPath
		End If
		If Err Then Exit Function

		' Angaben zur Quelle in Pfad und Dateiname aufteilen
		strHostPath = sobjApp.FSO.GetParentFolderName( strHostFilePath )		
		strHostMask = sobjApp.FSO.GetFileName( strHostFilePath )		

		' jetzt geht der FTP los
		sobjApp.Log.Write vbNewLine & "Start receive from remote side" & vbNewLine
		
		' FTP Kommandos definieren
		' Es werden alle Dateien via MGET auf einmal abgeholt und in einem
		' Temp-Verzeichnis abgelegt.
		' Es darf NIE die option "-be" (continue batch processing on errors) in FTPcmd
		' verwendet werden!
		strCmdList = "cd """ & strHostPath & """" & vbNewLine & _
					 "lcd """ & strTempPath & """" & vbNewLine & _
					 "mget """ & strHostMask & """" & vbNewLine & _
					 "bye" & vbNewLine & _
					 "quit" & vbNewLine
			
		' FTP ausführen
		If Not FTPcmd( strCmdList ) Or Err Then
		   Exit Function
		End If

		' Wurden alle FTP Befehle abgearbeitet?
		If InStr( FtpCodeList, "cd;lcd;mget;bye" ) <= 0 Then
 		    sobjApp.Log.Write vbNewLine & _
			 		 "+++ Transfer NOT successfully completed" & _
					 vbNewLine
			Err.Raise vbObjectError + 50, "", _
					  "Transfer Error"
		   Exit Function
		End If

		' Jetzt aus dem Rückmeldetext die übertragenen Dateien ermitteln
		strLines = Split( FtpText, vbNewLine )
		blnMGET = False
        Set objFileList = CreateObject( "Scripting.Dictionary" )
        objFileList.CompareMode = vbTextCompare
		For Each strLine In strLines
			' Erst beginnen wenn der "Start-Text" gefunden wurde!
			If blnMGET Then
				' wenn am Anfang "remote:" erscheint sind in der Zeile die Dateinamen
				' enthalten
				If Left( strLine, 7 ) = "remote:" Then
					' der lokale Dateiname steht dann hinter "local:"
					strLocalFile = Trim( Split( strLine, "local:" )(1) )
					If strLocalFile <> "" Then
						' noch einmal eine Prüfung ob die lokale Datei auch vorhanden ist
						If sobjApp.FSO.FileExists( strTempPath & "\" & strLocalFile ) Then
							' dann in der Dateiliste mit aufnehmen
							objFileList( strLocalFile ) = ""
							sobjApp.Log.Write " file received: " & strLocalFile
						End If
					End If
				End If
			
			Else
				' "Start-Text" vorhanden?
				If Left( strLine, 12 ) = "psftp> mget " Then
					blnMGET = True
				End If
			End If
		Next

		intFile = 0
		intTransfered = 0
		intDeleted = 0
		
		' Jetzt jede gefundene Datei einzeln weiter verarbeiten
		For Each strFile In objFileList
			intFile = intFile + 1
			
			' Zuerst wird die Datei auf dem FTP-Server gelöscht
			sobjApp.Log.Write vbNewLine & "Start delete on remote side: " & strFile

			' FTP Kommandos definieren
			' Es wird bei jeder Datei eine neue FTP-Session genutzt!
			' Es darf NIE die option "-be" (continue batch processing on errors) in FTPcmd
			' verwendet werden!
			strCmdList = "cd """ & strHostPath & """" & vbNewLine & _
						 "del """ & strFile & """" & vbNewLine & _
						 "bye" & vbNewLine & _
						 "quit" & vbNewLine
			
			' FTP ausführen			
			If FTPcmd( strCmdList ) And Err = 0 Then
			
				' Wurden alle FTP Befehle abgearbeitet?
				If InStr( FtpCodeList, "cd;del;bye" ) > 0 Then
				
					intDeleted = intDeleted + 1
					
					' wenn die Datei auf dem FTP-Server erfolgreich gelöscht wurde,
					' wird sie ins eigentlich Ziel-Verzeichnis verschoben.
					sobjApp.Log.Write vbNewLine & " move to local process path : " & strFile
					sobjApp.FSO.MoveFile strTempPath & "\" & strFile, strDestPathAndPrefix & strFile
					If Err = 0 Then
						intTransfered = intTransfered + 1
					Else
						sobjApp.Log.Write "+++ Error by local move."
					End If
					
				Else
					sobjApp.Log.Write "+++ Error by delete on remote side."
				End If
			Else
				sobjApp.Log.Write "+++ Error by delete on remote side."
			End If
			
		Next
		
		If intFile <> intTransfered Then
			sobjApp.Log.Write vbNewLine & _
					 "+++ Transfer NOT successfully completed" & _
					 vbNewLine
			Err.Raise vbObjectError + 50, "", _
					  "Transfer Error"
		End If
		
	End Function
		
	'----------------------------------------------------------------------------------------------
	' Eine Datei senden
	'
	Public Function FTPsend( strSourceFile, strDestFile )
		On Error Resume Next

		Dim strDestPath
		Dim strDestName
		Dim strDestExtension
		Dim strCmdList
		Dim strTempName
		Dim strTempPrefix
		Dim strTempSuffix
		
		' Angaben zum Ziel in Pfad, Dateiname und Dateiendung aufteilen
		strDestPath = sobjApp.FSO.GetParentFolderName( strDestFile )		
		strDestName = sobjApp.FSO.GetBaseName( strDestFile )		
		strDestExtension = sobjApp.FSO.GetExtensionName( strDestFile )
		If strDestExtension <> "" Then strDestExtension = "." & strDestExtension
		
		' Temp-Prefix und -Suffix definieren oder aus dem Aufrufparametern übernehmen
		strTempPrefix = sobjApp.Arg( "TempPrefix" )
		strTempSuffix = sobjApp.Arg( "TempSuffix" )
		If strTempPrefix = "" Then strTempPrefix = "temp_"
		If strTempSuffix = "" Then strTempSuffix = ".tmp"
		strTempName = strTempPrefix & strDestName & strTempSuffix

		' FTP Kommandos definieren
		' Es darf NIE die option "-be" (continue batch processing on errors) in FTPcmd
		' verwendet werden!
		' Die Datei wird zuerst via einem Temp-Namen übertragen und sofort im Anschluss
		' (in der gleichen Session) in dem Orginal-Namen umbenannt!
		strCmdList = "cd """ & strDestPath & """" & vbNewLine & _
					 "pwd" & vbNewLine & _
					 "put """ & strSourceFile & """ """ & strTempName & """" & vbNewLine & _
					 "ren """ & strTempName & """ """ & strDestName & strDestExtension & """" & vbNewLine & _
					 "bye" & vbNewLine & _
					 "quit" & vbNewLine
		
		' FTP ausführen
		If Not FTPcmd( strCmdList ) Or Err Then
		   Exit Function
		End If

		' Wurden alle FTP Befehle abgearbeitet?
		If InStr( FtpCodeList, "pwd;put;ren;bye" ) > 0 Then
		   sobjApp.Log.Write vbNewLine & _
					"...Transfer successfully completed." & _
					vbNewLine
		   Exit Function
		End If

		' Wenn ein Fehler erkannt wurde, dann wird ggf. versucht die Temp-Datei 
		' im Ziel wieder zu löschen.
		sobjApp.Log.Write vbNewLine & "+++ Transfer NOT successfully completed."
		If InStr( FtpText, "Failed to connect to" ) > 0 Then
			sobjApp.Log.Write "    (Connection Error)"			 
		Else
			sobjApp.Log.Write vbNewLine & "Start delete temp-file..."			 
			strCmdList = "cd """ & strDestPath & """" & vbNewLine & _
						 "pwd" & vbNewLine & _
						 "del """ & strTempName & """" & vbNewLine & _
						 "bye" & vbNewLine & _
						 "quit" & vbNewLine
			If Not FTPcmd( strCmdList ) Or Err Then
			   Exit Function
			End If
		End If
				 
		Err.Raise vbObjectError + 50, "", _
				  "Transfer Error"
				 
	End Function
	
	'----------------------------------------------------------------------------------------------
    ' Hiermit wird der eigentliche FTP gestartet
	'
	Public Function FTPcmd( strCmdList )
		On Error Resume Next
		
		Dim intStartTime
		Dim objFtpCmdFile
		Dim strParam
		Dim intWaitTime
		Dim intLoop
		Dim objExec
		Dim strText
		Dim strArray
		Dim strLine
		Dim strTemp
		Dim strCodeList
		Dim blnError
		
		intStartTime = Now
		blnError = False
		
		FtpText = ""
		FtpCodeList = ""

		CmdFile = sobjApp.RootPath & "\work\" & sobjApp.Arg( "WFID" )  & ".ftp"
		
		If sobjApp.FSO.FileExists( CmdFile ) Then
		   sobjApp.FSO.DeleteFile CmdFile, True
		End If
		Set objFtpCmdFile = sobjApp.FSO.OpenTextFile( CmdFile, sobjApp.File.ForWriting, True )
		objFtpCmdFile.Write strCmdList
		objFtpCmdFile.Close
		If Err Then
		   Err.Raise vbObjectError + 30, "", _
					 "Cannot create: " &  CmdFile & vbNewLine & _
					 "               ( " & Err.Number & " - " & Err.Description & " )"
		   FTPcmd = False
		   Exit Function
		End If

		sobjApp.Log.Write "FTP-CommandList: " & CmdFile
		
		sobjApp.Log.PrefixText = " > "
		sobjApp.Log.Write strCmdList
		sobjApp.Log.PrefixText = ""

		sobjApp.Log.Write vbNewLine & "Start transfer..."

		' +++ DON'T USE param "-be" because it is continue batch processing on errors +++
		' +++ In this Script we check the status of a FTP session via used command list! +++
		strParam = " -v -l " & User & " -pw """ & PW & """ " & Host & _
				   " -batch -bc -b """ & CmdFile  & """"
		sobjApp.Log.Write "(" & FTPEXE & Replace( strParam, PW, "********" ) & ")"

		WScript.Echo "Start " & FTPEXE & "..."
		
		Set objExec = sobjApp.Shell.Exec( "%comspec% /c " & FTPEXE & strParam )
		intWaitTime = 0
		intLoop = 0
		strText = ""
		Do While objExec.Status = 0
		   strText = strText & vbNewLine & objExec.StdOut.ReadAll
		   If intWaitTime >= 50 Then
			  WScript.Echo "...wait ... " & Now & " ( " & Abs( DateDiff( "s", intStartTime, Now )) & " seconds )"
			  intWaitTime = 0
			  intLoop = intLoop + 1
		   End If
		   WScript.Sleep 100
		   intWaitTime = intWaitTime + 1
		   If intLoop > 5 Then
			   Err.Raise vbObjectError + 35, "", _
						 "Waiting error: " &  CmdFile & vbNewLine & _
						 "               ( " & Err.Number & " - " & Err.Description & " )"
			   Exit Do
		   End If
		Loop

		If Not objExec.StdErr.AtEndOfStream Then
			strText = objExec.StdErr.ReadAll & vbNewLine & strText
		End If
		If Not objExec.StdOut.AtEndOfStream Then
			strText = strText & vbNewLine & objExec.StdOut.ReadAll
		End If

		WScript.Echo "...ended."

		blnError = blnError Or ( Err <> 0 ) ' merken da beim LogWrite der Error gelöscht wird
		
		sobjApp.Log.PrefixText = " > "
		sobjApp.Log.Write strText
		sobjApp.Log.PrefixText = ""
		FtpText = strText

		strArray = Split( strText, vbNewLine )
		For Each strLine In strArray
			strTemp = Split( strLine, " " )
			If UBound( strTemp ) >= 1 Then
			   If strTemp( 0 ) = "psftp>" Then
				  strCodeList = strCodeList & ";" & strTemp( 1 )
			   ElseIf IsNumeric( strTemp( 0 )) Then
				  strCodeList = strCodeList & "," & strTemp( 0 )
			   End If
			End If
		Next
		sobjApp.Log.PrefixText = " >>> "
		sobjApp.Log.Write "CodeList=" & strCodeList
		sobjApp.Log.PrefixText = ""
		FtpCodeList = strCodeList

		If sobjApp.FSO.FileExists( CmdFile ) Then
			sobjApp.FSO.DeleteFile CmdFile, True
		End If

		If Err = 0 And blnError Then
			Err.Raise vbObjectError + 35, "", ""
		End If
		
		FTPcmd = ( Err = 0 )
		
	End Function

	'----------------------------------------------------------------------------------------------
    
End Class ' PSFTP

'* ************************************************************************************************************************
'* Hilfsfuntionen
'*

Function IFF( blnIf, strThen, strElse )
    On Error Resume Next

	If blnIf Then
		IFF = strThen
	Else
		IFF = strElse
	End If
End Function

'* ************************************************************************************************************************
'* Anwendungsübergreifene Funktionen
'*

Class WApplication

	Public FSO
	Public Shell
	Public ScriptPath
	Public RootPath
	Public Name
	Public Log
	Public File
	Public Arg
	Public Env
	
	'----------------------------------------------------------------------------------------------

    Private Sub Class_Initialize()
        On Error Resume Next

    End Sub

	'----------------------------------------------------------------------------------------------

    Private Sub Class_Terminate()
        On Error Resume Next

    End Sub

	'----------------------------------------------------------------------------------------------

    Public Function init()
        On Error Resume Next

		Set FSO = CreateObject( "Scripting.FileSystemObject" ) 
			' http://msdn.microsoft.com/en-us/library/z9ty6h50(VS.85).aspx
			
		Set Shell = CreateObject( "WScript.Shell" ) 
			' http://msdn.microsoft.com/en-us/library/aew9yb99(VS.85).aspx

		ScriptPath = FSO.GetParentFolderName( WScript.ScriptFullName )
		RootPath = FSO.GetParentFolderName( ScriptPath )
		Name = FSO.GetBaseName( WScript.ScriptFullName )
		
		Set Log = New WLogging 
		Set File = New WFileHandling
		
		Set Arg = WScript.Arguments.Named
		
		If Arg( "WfID" ) <> "" Then
			Log.Name = Arg( "WfID" )
		End If

		If Arg( "LogFile" ) <> "" Then
			Log.FilePath = Arg( "LogFile" )
		End If
		
		Set Env = Shell.Environment( "SYSTEM" )
		
    End Function

	'----------------------------------------------------------------------------------------------

    Public Function PrintStartInfo()
        On Error Resume Next

		Dim objFile
		Dim objArgs
		Dim intPos
		
		Set objFile = FSO.GetFile( WScript.ScriptFullName )
		
		Log.Write "" _
			& "Script details" & vbNewLine _
			& String( 80, "-" ) & vbNewLine _
			& "Engine        : " & WScript.Name & " " & WScript.Version & " " & WScript.BuildVersion & vbNewLine _
			& "File          : " & WScript.ScriptFullName & vbNewLine _
			& "Last modified : " & objFile.DateLastModified

		Set objArgs = WScript.Arguments
		Log.Write "" _
			& "Parameter     : " & objArgs.Count
		For intPos = 0 to objArgs.Count - 1
		   Log.Write "              " & Right( "     " & intPos, 5 ) & " = [" &  objArgs( intPos ) & "]"
		Next
		Log.Write vbNewLine & String( 80, "-" ) & vbNewLine
		
    End Function

	'----------------------------------------------------------------------------------------------
    
    Public Function DosCmd( strCmd )
        On Error Resume Next

		Dim objExec
		Dim strOut
		Dim strErr
		
		Set objExec = Shell.Exec( "%COMSPEC% /c " & strCmd )

		If Not objExec.StdOut.AtEndOfStream Then
			strOut = objExec.StdOut.ReadAll
		End If
		
		If Not objExec.StdErr.AtEndOfStream Then
			strErr = objExec.StdErr.ReadAll
		End If
		
		If Err Then
			Err.Raise Err.Number, Err.Source, _
				Err.Description & vbNewLine & _
				"DosCmd: " & strCmd & vbNewLine & _
				"StrOut: " & strOut & vbNewLine & _
				"StdErr: " & strErr
			DosCmd = ""
			Exit Function
		End If
		
		If strErr <> "" Then
			Err.Raise 1001, "DosCmd", vbNewLine & _
				"DosCmd: " & strCmd & vbNewLine & _
				"StrOut: " & strOut & vbNewLine & _
				"StdErr: " & strErr
			DosCmd = ""
			Exit Function
		End If
		
		DosCmd = strOut
		
    End Function
	
	'----------------------------------------------------------------------------------------------
	
End Class ' WApplication

'* ************************************************************************************************************************
'* Dateizugriffe
'*

Class WFileHandling

	Public ForReading
	Public ForWriting
	Public ForAppending

	'----------------------------------------------------------------------------------------------

    Private Sub Class_Initialize()
        On Error Resume Next

		ForReading = 1
		ForWriting = 2
		ForAppending = 8
		
    End Sub

	'----------------------------------------------------------------------------------------------

    Private Sub Class_Terminate()
        On Error Resume Next

    End Sub

	'----------------------------------------------------------------------------------------------

	Public Function Create( strFile, strText )
		On Error Resume Next
		
		Dim objFile
		
		Set objFile = sobjApp.FSO.OpenTextFile( strFile, sobjApp.File.ForWriting, True )
		If Err = 0 Then
			objFile.Write strText
			objFile.Close
		End If

		If Err Then
			Err.Raise Err.Number, Err.Source, _
				Err.Description & vbNewLine & _
				"    FileCreate: " & strFile
		End If
		
	End Function
	
	'----------------------------------------------------------------------------------------------

	Public Function Copy( strSource, strDestination )
		On Error Resume Next
	
		If Not sobjApp.FSO.FileExists( strSource ) Then
			Exit Function
		End If
		
		sobjApp.FSO.CopyFile strSource, strDestination, True

		If Err Then
			Err.Raise Err.Number, Err.Source, _
				Err.Description & vbNewLine & _
				"    FileCopy: " & strSource & vbNewLine & _
				"          to: " & strDestination
		End If
		
	End Function
	
	'----------------------------------------------------------------------------------------------

	Public Function LikeMask( strFileName, strFileMask )
		On Error Resume Next

		Dim objRegExp, strMask
		Set objRegExp = New RegExp

		strMask = Replace( strFileMask, "\", "\\" )
		strMask = Replace( strMask, ".", "\." )
		strMask = Replace( strMask, "*", ".*" )
		strMask = Replace( strMask, "?", ".{1}" )

		strMask = Replace( strMask, "$", "\$" )
		strMask = Replace( strMask, "^", "\^" )

		objRegExp.Pattern = "^" & strMask & "$"
		objRegExp.IgnoreCase = True
        
		LikeMask = objRegExp.Test( strFileName )

	End Function

	'----------------------------------------------------------------------------------------------

End Class ' WFileHandling

'* ************************************************************************************************************************
'* Protokollierung
'*

Class WLogging

	Public FilePath
	Public Path
	Public Name
	Public Extension
	Public ErrorPrefixText
    Public PrefixText
    Public IgnoreTimeStamp

	'----------------------------------------------------------------------------------------------

    Private Sub Class_Initialize()
        On Error Resume Next

		Path = sobjApp.RootPath &  "\log" 
		Name = sobjApp.Name
		Extension = ".log"
		
		ErrorPrefixText = "+++ "
        IgnoreTimeStamp = False

    End Sub

	'----------------------------------------------------------------------------------------------

    Private Sub Class_Terminate()
        On Error Resume Next

    End Sub

	'----------------------------------------------------------------------------------------------

    Public Function DateTime( dtmValue, strDelimiter )
        On Error Resume Next

		Dim D
		Dim S
		Dim T
		
		D = Mid( strDelimiter, 1, 1 )
		S = Mid( strDelimiter, 2, 1 )
		T = Mid( strDelimiter, 3, 1 )
		
		DateTime = 	             Year(   dtmValue )      & D & _
					Right("00" & Month(  dtmValue ), 2 ) & D & _
					Right("00" & Day(    dtmValue ), 2 ) & S & _
					Right("00" & Hour(   dtmValue ), 2 ) & T & _
					Right("00" & Minute( dtmValue ), 2 ) & T & _
					Right("00" & Second( dtmValue ), 2 )
		
    End Function

	'----------------------------------------------------------------------------------------------
	
	Public Function Write( strText )

		Dim strFile
		Dim objFile
		Dim strArray
		Dim strLine
		Dim strLines
		Dim strTimeStamp
		
		If Err Then
			strLines = strText & vbNewLine & _
					   ErrorPrefixText & "Error=" & Err.Number & ", " & Err.Source & ", " & Err.Description
		Else
			strLines = strText
		End If
		
		On Error Resume Next

		If FilePath = "" Then
			strFile = Path & "\" & Name & Extension
		Else
			strFile = FilePath
		End If
		Set objFile = sobjApp.FSO.OpenTextFile( strFile, sobjApp.File.ForAppending, True )

        If IgnoreTimeStamp = False Then
            strTimeStamp = DateTime( Now, "- :" ) & " | "
        End If
		strArray = Split( strLines, vbNewLine )
		For Each strLine In strArray
			WScript.Echo PrefixText & strLine
			objFile.WriteLine strTimeStamp & PrefixText & strLine
		Next
		objFile.Close

		If Err = 0 Then		
			Write = True
		Else
			Write = False
		End If

	End Function

	'----------------------------------------------------------------------------------------------
	
End Class ' WLogging

'* ************************************************************************************************************************
'* Datenbank-Handling
'*

Class WOLEDB

    Public Connection

    Public adOpenStatic
    Public adOpenForwardOnly
    Public adOpenDynamic
    Public adOpenKeyset

    Public adLockReadOnly
    Public adLockPessimistic
    Public adLockOptimistic
    Public adLockBatchOptimistic
	Public adClipString 
	
	Public adStateClosed

	'----------------------------------------------------------------------------------------------
	
    Private Sub Class_Initialize()
        On Error Resume Next

        Set Connection = Nothing

        adOpenStatic = 3
        adOpenForwardOnly = 0
        adOpenDynamic = 2
        adOpenKeyset = 1

        adLockReadOnly = 1
        adLockPessimistic = 2
        adLockOptimistic = 3
        adLockBatchOptimistic = 4
		adClipString = 2

        adStateClosed = 0
		
    End Sub


    Private Sub Class_Terminate()
        On Error Resume Next

        Close

    End Sub

	'----------------------------------------------------------------------------------------------

    Public Function OpenUDL( strUDLName )
        On Error Resume Next

        Dim strConnectionString

        Close

        strConnectionString = "File Name=" & sobjApp.ScriptPath & "\" & strUDLName & ".udl;"

		Set Connection = CreateObject( "ADODB.Connection" ) 
			' http://msdn.microsoft.com/en-us/library/ms681519.aspx
			
        Connection.Open strConnectionString
			
        If Err = 0 Then
			OpenUDL = True
        Else
			Err.Raise Err.Number, Err.Source, _
				Err.Description & vbNewLine & _
				"    Connection string: " & strConnectionString
			OpenUDL = False
        End If

    End Function

	'----------------------------------------------------------------------------------------------

    Public Function Close()
        On Error Resume Next

        If Connection Is Nothing Then
           Exit Function
        End If

        If Connection.State = adStateClosed Then
           Exit Function
        End If

        Connection.Close
        Set Connection = Nothing

    End Function

	'----------------------------------------------------------------------------------------------

    Public Function ExistsTable( strTable )
        On Error Resume Next

        Dim rst

        Set rst = OpenRecordset( "SELECT * FROM sysobjects WHERE name = '" & _
                  strTable & "' and xtype = 'U'", False )

        ExistsTable = ( Not rst.EOF )
        rst.Close

    End Function

	'----------------------------------------------------------------------------------------------

    Public Function OpenRecordset( strSQL, blnReadAndWrite )
        On Error Resume Next

        Dim rs

		'sobjApp.Log.Write "Open recordset = " & strSQL
		
        Set rs = CreateObject( "ADODB.Recordset" )
        rs.ActiveConnection = Connection

        If rs Is Nothing Then

        ElseIf blnReadAndWrite Then
            rs.Open strSQL, , adOpenDynamic, adLockOptimistic
        Else
            rs.Open strSQL
        End If

        If Err = 0 Then
            Set OpenRecordset = rs
        Else
			Err.Raise Err.Number, Err.Source, _
				Err.Description & vbNewLine & _
				"    SQL string: " & strSQL
            Set OpenRecordset = Nothing
        End If

    End Function

	'----------------------------------------------------------------------------------------------

	Public Function RecordToText( objRecordset )
		On Error Resume Next
		
		Dim objField
		Dim strText
		
		For Each objField In objRecordset.Fields
			strText = strText & objField.Name & "=[" & objField.Value & "]" & vbNewLine
		Next
		
		RecordToText = strText
	
	End Function

	'----------------------------------------------------------------------------------------------

	Public Function RecordToTextContent( objRecordset )
		On Error Resume Next
		
		Dim objField
		Dim strText
		
		For Each objField In objRecordset.Fields
            If objField.Value <> "" Then
                strText = strText & objField.Name & "=[" & objField.Value & "]" & vbNewLine
            End If
		Next
		
		RecordToTextContent = strText
	
	End Function

	'----------------------------------------------------------------------------------------------

	Public Function GetValue( rstSource, strFieldName )

		Dim strValue

		If Err Then
			GetValue = ""
			Exit Function
		End If

		On Error Resume Next
		
		strValue = rstSource( strFieldName ).Value
		
		If Err Then
			GetValue = ""
			Err.Raise Err.Number, Err.Source, _
				Err.Description & vbNewLine & _
				"    Fieldname: " & strFieldName
			Exit Function
		End If
	
		If IsNull( strValue ) Then
			strValue = ""
		End If
		
		GetValue = strValue
	
	End Function
    
	'----------------------------------------------------------------------------------------------
	
End Class ' WOLEDB

'* ************************************************************************************************************************
'* Sperrdatei-Handling
'*

Class LockFile

    ' -------------------------------------------------------------------------

    Private pstrLockFile
    Private pobjLockFile

    Public DeleteByEnd

    ' -------------------------------------------------------------------------

    Private Sub Class_Initialize()
        On Error Resume Next

        Set pobjLockFile = Nothing
        DeleteByEnd = True

    End Sub

    ' -------------------------------------------------------------------------

    Private Sub Class_Terminate()
        On Error Resume Next

        Close

    End Sub

    ' -------------------------------------------------------------------------

    Public Function Open( strLockFile, strText )
        On Error Resume Next

        Close

        pstrLockFile = strLockFile

        Set pobjLockFile = sobjApp.FSO.OpenTextFile( pstrLockFile, _
                           sobjApp.File.ForAppending, True )

        If pobjLockFile Is Nothing Then
            If Err = 70 Then ' Permission denied 
                Err.Clear
                sobjApp.Log.Write "*** Permission denied "
            End If
            Open = False
            Exit Function
        End If

        If strText <> "" Then
           pobjLockFile.WriteLine strText
        End If

        Open = ( Err = 0 )

    End Function

    ' -------------------------------------------------------------------------

    Public Function Close()
        On Error Resume Next

        If pobjLockFile Is Nothing Then
            Exit Function
        End If

        pobjLockFile.Close
        Set pobjLockFile = Nothing

        If DeleteByEnd Then
           sobjApp.FSO.DeleteFile pstrLockFile, True
        End If

    End Function

    ' -------------------------------------------------------------------------

End Class ' LockFile

'* ************************************************************************************************************************
